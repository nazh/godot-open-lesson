# lesson 3

extends AcceptDialog

# member variables here, example:
# var a=2
# var b="textvar"

func _ready():
	# Initalization here
	set_hide_on_ok ( true )

func reset_game():
	self.hide()

func _on_YouWonAlert_confirmed():
	OS.get_main_loop().quit()
