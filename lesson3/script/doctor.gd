# lesson 3

extends Area2D

# member variables here, example:
# var a=2
# var b="textvar"

var step = 7
var max_x
var max_y

var orig_pos

func _ready():
	# Initalization here
	set_process_input(true)
	set_process(true)
	orig_pos = self.get_pos()

func reset_game():
	print ("resetting the doctor")
	self.set_pos(orig_pos)
	step = 7

func _process(delta):
	if (self.get_node("/root/Background").turn == 0):
		if (self.get_overlapping_bodies().size() > 0):
			self.get_node("/root/Background/GameOverAlert").popup()
			self.get_node("/root/Background").turn = -2 # no body moves

func _input(event):
	var pos = self.get_pos()
	var bg_size = self.get_node("/root/Background").get_size()
	max_x = bg_size.x
	max_y = bg_size.y
	if (self.get_node("/root/Background").turn == 0):
		if (Input.is_action_pressed ("north")):
			pos.y -= step
			if (pos.y < 0):
				pos.y = max_y
			doc_move_done("north", pos)
		if (Input.is_action_pressed ("south")):
			pos.y += step
			if (pos.y > max_y):
				pos.y = 0
			doc_move_done("south", pos)
		if (Input.is_action_pressed ("east")):
			pos.x -= step
			if (pos.x < 0):
				pos.x = max_x
			doc_move_done("west", pos)
		if (Input.is_action_pressed ("west")):
			pos.x += step
			if (pos.x > max_x):
				pos.x = 0
			dalek_move_setup("east", pos)
		
func doc_move_done (dir, new_pos):
	print ("going ", dir, " new coord: ", new_pos)
	self.set_pos(new_pos)
	self.get_node("/root/Background").turn = 1
	print ("dalek's turn ")
