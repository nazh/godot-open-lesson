# lesson 3

extends KinematicBody2D

# member variables here, example:
# var a=2
# var b="textvar"

var step = 10 # make dalek faster than doctor
var I_moved = false
var collided = false

var orig_pos
var dm

func _ready():
	dm = self.get_node("/root/Background")
	# Initalization here
	set_process(true)
	orig_pos = self.get_pos()
	move_to(orig_pos)
	dm.num_daleks += 1
	dm.daleks_ready += 1

func reset_game():
	self.set_pos(orig_pos)
	step = 10
	I_moved = false
	collided = false
	
func collide_react():
	if (not collided):
		print (get_name(), "collide react activate")
		dm.daleks_stuck += 1
		collided = true
		step = 0

func _process(delta):
	if (dm.turn == 1):
		if (not I_moved):
			var doctor_pos = self.get_node("/root/Background/doctor").get_pos()
			var diff = doctor_pos - self.get_pos()
			var move_vec = diff.normalized() * step
			move(move_vec)
			print(get_name(), " is colliding? ",self.is_colliding())
			if ((self.is_colliding()) and (not collided)):
				var colide_obj = get_collider()
				print (get_name(), "colliding")
				print ("Collision with ", colide_obj.get_name())
				print ("calling the other object's collide_react function")
				colide_obj.collide_react()
				collide_react()
			I_moved = true
			dm.daleks_moved += 1
			print (dm.daleks_moved, " dalek moved")
			
		if (dm.daleks_moved == dm.num_daleks):
			dm.daleks_ready = 0
			dm.daleks_moved = 0
			dm.turn = -1
		if (dm.num_daleks == dm.daleks_stuck):
			print("all dalek stuck")
			dm.turn = -2 # no body moves
			self.get_node("/root/Background/YouWonAlert").popup()
	
	if (dm.turn == -1):
		I_moved = false
		dm.daleks_ready += 1
		if (dm.num_daleks == dm.daleks_ready):
			print ("now doctor's turn")
			dm.turn = 0
