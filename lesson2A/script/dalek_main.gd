
extends TextureFrame

# member variables here, example:
# var a=2
# var b="textvar"

export var turn = 0 # 0 for doctor 1 for dalek -1 for daleks resetting for next turn
export var num_daleks = 0
export var daleks_moved = 0
export var daleks_ready = 0
export var daleks_stuck = 0

func _ready():
	# Initalization here
	pass
