# lesson 2A

extends KinematicBody2D

# member variables here, example:
# var a=2
# var b="textvar"

var step = 10 # make dalek faster than doctor
var I_moved = false
var collided = false

func _ready():
	var dm = self.get_node("/root/Background")
	# Initalization here
	set_process(true)
	move_to(self.get_pos())
	dm.num_daleks += 1
	dm.daleks_ready += 1


func _process(delta):
	var dm = self.get_node("/root/Background")
	if (dm.turn == 1):
		if (not I_moved):
			var doctor_pos = self.get_node("/root/Background/doctor").get_pos()
			var diff = doctor_pos - self.get_pos()
			var move_vec = diff.normalized() * step
			move(move_vec)
			print(get_name(), " is colliding? ",self.is_colliding())
			if ((self.is_colliding()) and (not collided)):
				print (get_name(), "colliding")
				dm.daleks_stuck += 1
				collided = true
				step = 0
			I_moved = true
			dm.daleks_moved += 1
			print (dm.daleks_moved, " dalek moved")
			
		if (dm.daleks_moved == dm.num_daleks):
			dm.daleks_ready = 0
			dm.daleks_moved = 0
			dm.turn = -1
			
		if (dm.num_daleks == dm.daleks_stuck):
			print("all dalek stuck")
			dm.turn = -2 # no body moves
			OS.get_main_loop().quit()
	
	if (dm.turn == -1):
		I_moved = false
		dm.daleks_ready += 1
		if (dm.num_daleks == dm.daleks_ready):
			print ("now doctor's turn")
			dm.turn = 0
