#lesson 1

extends Area2D

# member variables here, example:
# var a=2
# var b="textvar"

var step = 7
var max_x
var max_y

func _ready():
	# Initalization here
	set_process_input(true)


func _input(event):
	var pos = self.get_pos()
	var bg_size = self.get_node("/root/Background").get_size()
	max_x = bg_size.x
	max_y = bg_size.y
	if (Input.is_action_pressed ("north")):
		pos.y -= step
		if (pos.y < 0):
			pos.y = max_y
		print ("going north, new coord: ", pos)
		doc_move_done(pos)
	if (Input.is_action_pressed ("south")):
		pos.y += step
		if (pos.y > max_y):
			pos.y = 0
		print ("going south, new coord: ", pos)
		doc_move_done(pos)
	if (Input.is_action_pressed ("east")):
		pos.x -= step
		if (pos.x < 0):
			pos.x = max_x
		print ("going west, new coord: ", pos)
		doc_move_done(pos)
	if (Input.is_action_pressed ("west")):
		pos.x += step
		if (pos.x > max_x):
			pos.x = 0
		print ("going east, new coord: ", pos)
		doc_move_done(pos)
		
func doc_move_done (new_pos):
	self.set_pos(new_pos)
